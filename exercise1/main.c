/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/04/2020
 */

#include <stdio.h>

int gcd(int a, int b);
int even(int value);

int main() {
    int a, b, ret;
    // Richiedo il primo numero.
    printf("Inserire il primo numero intero: ");
    ret = scanf("%d", &a);

    if (ret != 1) {
        printf("Numero non valido!");
        return -1;
    }

    // Richiedo il secondo numero.
    printf("Inserire il secondo numero intero: ");
    ret = scanf("%d", &b);

    if (ret != 1) {
        printf("Numero non valido!");
        return -1;
    }

    // Stampo l'output della funzione mcd.
    printf("Il MCD tra %d e %d è: %d", a, b, gcd(a, b));

    return 0;
}

int gcd(int a, int b)
{
    if (a <= 0) {
        return b;
    }

    if (b <= 0) {
        return a;
    }

    int tmp;
    // Swap se a < b.
    if (a < b) {
        tmp = a;
        a = b;
        b = tmp;
    }

    // Applico la definizione ricorsiva trattata in consegna.
    if (even(a)) {
        if (even(b)) {
            return 2 * gcd(a/2, b/2);
        } else {
            return gcd(a/2, b);
        }
    } else {
        if (even(b)) {
            return gcd(a, b/2);
        } else {
            return gcd((a-b)/2, b);
        }
    }
}

// Verifica se il valore di `value` è pari.
int even(int value)
{
    return value % 2 == 0;
}