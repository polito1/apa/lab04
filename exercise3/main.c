/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/17/2020
 */

#include <stdio.h>
#include <ctype.h>
#include <string.h>

// Massima lunghezza delle stringhe
#define MAX_STRING_LENGTH 50

char *cercaRegexp(char *src, char *regexp);

int main() {
    char src[MAX_STRING_LENGTH], regExp[MAX_STRING_LENGTH];
    printf("Inserire una stringa da analizzare (max %d caratteri): ", MAX_STRING_LENGTH);
    fgets(src, MAX_STRING_LENGTH, stdin);
    printf("Inserire la regex con cui filtrare (max %d caratteri): ", MAX_STRING_LENGTH);
    fgets(regExp, MAX_STRING_LENGTH, stdin);

    char *foundRegex = cercaRegexp(src, regExp);

    // Se non ho trovato alcuna corrispondenza lo comunico all'utente
    if (foundRegex == NULL) {
        printf("Regex non trovata.");
        return 0;
    }
    // Se invece ho trovato una corrispondenza, stampo il carattere ritornato.
    printf("Risultato: %c", *foundRegex);
    return 0;
}

char * cercaRegexp(char *src, char *regexp)
{
    char srcChar;
    int found, subFound, k, l;

    // Per ogni carattere della stringa
    for (int i = 0; i < strlen(src); i++) {
        found = 1;
        l = 0;

        // Se è un carattere di spaziatura, siccome non sono considerati (come da consegna), salto.
        if (isspace(src[i])) {
            continue;
        }

        // Controllo se la regex è trovata a partire dal carattere corrente.
        for (int j = 0; j < strlen(regexp); j++) {
            srcChar = src[i + l];

            if (regexp[j] == '.') {
                if (!isalpha(srcChar)) {
                    found = 0;
                }
                l++;
            } else if (regexp[j] == '[') {
                subFound = 0;
                if (regexp[j + 1] == '^') {
                    k = 1;
                    while (k < strlen(regexp) && regexp[k + j] != ']') {
                        if (regexp[k + j] == srcChar) {
                            subFound = 1;
                        }

                        k++;
                    }

                    j += k;

                    if (subFound) {
                        found = 0;
                    }

                } else {
                    k = 1;

                    while (k < strlen(regexp) && regexp[k + j] != ']') {
                        if (regexp[k + j] == srcChar) {
                            subFound = 1;
                        }

                        k++;
                    }

                    j += k;

                    if (!subFound) {
                        found = 0;
                    }
                }

                l++;
            } else if (regexp[j] == '\\') {
                if (regexp[j + 1] == 'A') {
                    if (!isupper(srcChar)) {
                        found = 0;
                    }
                } else {
                    if (isupper(srcChar)) {
                        found = 0;
                    }
                }

                j++;
                l++;
            } else {
                if (srcChar != regexp[j]) {
                    found = 0;
                }
                l++;
            }
        }

        // Se ho trovato una corrispondenza, ritorno un puntatore al carattere di partenza della regex.
        if (found) {
            return &src[i];
        }
    }

    // Se non è stata trovata alcuna corrispondenza, ritorno NULL.
    return NULL;
}