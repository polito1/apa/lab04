/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 11/04/2020
 */

#include <stdio.h>

int majority(int *a, int n);
int recursiveMajority(int *a, int n, int l, int r);

int main() {
    int arrayLength = 7;
    int a[] = {3, 3, 9, 4, 3, 5, 3};

    printf("Majority: %d", majority(a, arrayLength));

    return 0;
}

// Funzione wrapper della funzione ricorsiva.
int majority(int *a, int n)
{
    int l = 0, r = n - 1;

    return recursiveMajority(a, n, l, r);
}

int recursiveMajority(int *a, int n, int l, int r)
{
    int majLeft, majRight, countLeft = 0, countRight = 0, middle;

    // Condizione di terminazione: un numero è sicuramente maggioritario di un vettore che contiene solo esso.
    if (r == l) {
        return a[r];
    }

    // Calcolo la posizione di mezzo
    middle = (l + r) / 2;

    // Richiamo ricorsivo della funzione sui due sottovettori di dimensione N/2
    majLeft = recursiveMajority(a, n, l, middle);
    majRight = recursiveMajority(a, n, middle + 1, r);

    // Se entrambi i sottovettori hanno in comune lo stesso maggiorante, posso ritornare il controllo
    // alla funzione chiamante
    if (majLeft == majRight) {
        return majLeft;
    }

    // Controllo se uno dei maggioritari dei due sottovettori di dimensione N/2 è maggioritario del vettore
    // su cui opera la funzione chiamante.
    for (int i = 0; i <= r; i ++) {
        if (majLeft == a[i]) {
            countLeft ++;
        }

        if (majRight == a[i]) {
            countRight ++;
        }
    }

    // Il maggioritario del sottovettore sinistro è maggioritario anche del vettore della funzione chiamante
    if (countLeft > (r-l+1)/2) {
        return majLeft;
    }

    // Il maggioritario del sottovettore destro è maggioritario anche del vettore della funzione chiamante
    if (countRight > (r-l+1)/2) {
        return majRight;
    }

    // Non ho trovato maggioritario: ritorno -1 come da consegna.
    return -1;
}